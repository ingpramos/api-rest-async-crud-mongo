const bodyParser = require('body-parser');
const morgan = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

const userRoutes = require('./routes/user_route');

// imporant put cors if you dont use cors, the app crash!

app.use(cors());

mongoose
  .connect(
    'mongodb://localhost/api-rest-async',
    {
      useNewUrlParser: true
    }
  )
  .then(db => console.log('db is connected'))
  .catch(err => console.log(err));

//settings

app.set('port', process.env.PORT || 3000);

//middleware

app.use(morgan('dev'));
app.use(bodyParser.json());

// routes

app.use('/users', userRoutes);

// static files

// app.use(express.static(path.join(__dirname, 'dist')));

// error handlers

//start the server

app.listen(app.get('port'), () => {
  console.log('server on port', app.get('port'));
});
